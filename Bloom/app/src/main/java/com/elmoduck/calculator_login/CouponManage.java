package com.elmoduck.calculator_login;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class CouponManage extends AppCompatActivity {
    RequestQueue siteRequest2;

    private static final String TAG = "MainActivity";

    List<String> couponList;//intentionally an empty string. Filled later
    List<String> couponDescriptionList;//intentionally an empty string. Filled later
    List<String> couponIDs;

    String phpUrl2;

    String company_name;
    String username;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_manage);

        //This is where we activate the lists defined just a few lines ago
        couponList = new ArrayList<>();
        couponDescriptionList = new ArrayList<>();
        couponIDs = new ArrayList<>();

        //Retrieve the intent that brought us here so that we can determine what data was passed along to this activity
        Intent manageIntent = getIntent();
        company_name = manageIntent.getStringExtra("company_name");//pull the value of the vendor id, which was converted to a string in the previous java class
        username = manageIntent.getStringExtra("username");

        email = manageIntent.getStringExtra("email");


        phpUrl2 = "http://cgi.soic.indiana.edu/~tygoblir/bloom/bloom_request_vendor_coupons_manage.php?vendorID=" + username;

        Log.d(TAG, "Build URL: " + phpUrl2);

        //This creates a new request for the database so that we can pull the location information
        siteRequest2 = Volley.newRequestQueue(this);

        Log.d(TAG, "We made it this far...");

        configureVendorCouponButton();
        configureVendorSettingsButton();

        //------------------------------This is what pulls location and bio data from the database----------------------------------

        //phpUrl is defined at the top of this page
        JsonObjectRequest jsonVendorRequest = new JsonObjectRequest(Request.Method.GET, phpUrl2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Even though this is higher in this document than some code, the code below this section will be executed before this is done looping.
                //you're required to make this a try/catch when dealing with json requests
                try {
                    JSONArray jsonArray = response.getJSONArray("coupons");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject coupons = jsonArray.getJSONObject(i);
                        Log.d(TAG, "JSON array: " + coupons);

                        //These variables create strings unique to this iteration. They pull the data from the database by referencing the actual header in the database (eg "name" is a header in the database listing)
                        String pulledCouponID = coupons.getString("coupon_id");
                        String pulledCoupon = coupons.getString("title");
                        String pulledDescription = coupons.getString("description");
                        String pulledPoints = coupons.getString("point_total");

                        couponIDs.add(i, pulledCouponID);
                        couponList.add(i, pulledCoupon + " - " + pulledPoints + " points");
                        couponDescriptionList.add(i, pulledDescription);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //This logs to the debugger what our lists look like
                Log.d(TAG, "coupon list: " + couponList);
                Log.d(TAG, "description list: " + couponDescriptionList);
                Log.d(TAG, "Coupon ID list: " + couponIDs);

                //After we have generated the lists, we can assign the listLocations to the listView
                ListView lv = findViewById(R.id.listOfCoupons);//Here we create a variable to store the listView by its id. This way we can assign it a list in the next line
                lv.setAdapter(new ArrayAdapter<>(CouponManage.this, android.R.layout.simple_list_item_1, couponList));

                //This function makes the bio button clickable, taking you to the bio
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        final int couponPosition = position;//we make this because within this chunk is how we find the coupon position. this value is needed in the next onClick

                        AlertDialog.Builder redeemAlert = new AlertDialog.Builder(CouponManage.this);
                        redeemAlert.setTitle("Bloom");
                        redeemAlert.setMessage("Are you sure you wish to delete coupon.");
                        redeemAlert.setCancelable(false);

                        redeemAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final String coupon_id = String.valueOf(couponIDs.get(couponPosition));

                                Response.Listener<String> responseListener = new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Intent couponDeactivateIntent = new Intent(CouponManage.this, CouponDeactivate.class);

                                        couponDeactivateIntent.putExtra("company_name", company_name);
                                        couponDeactivateIntent.putExtra("username", username);
                                        couponDeactivateIntent.putExtra("email", email);


                                        CouponManage.this.startActivity(couponDeactivateIntent);
                                    }
                                };

                                DeactivateCouponRequest deactivateCouponRequest = new DeactivateCouponRequest(coupon_id, username, responseListener);
                                RequestQueue queue = Volley.newRequestQueue(CouponManage.this);
                                queue.add(deactivateCouponRequest);
                            }
                        });

                        redeemAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //does nothing
                            }
                        });

                        AlertDialog alert = redeemAlert.create();
                        alert.show();

                        /*
                        final String username = intentUsername;
                        final String coupon_id = String.valueOf(couponIDs.get(position));

                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Intent couponConfirmIntent = new Intent(LocationSpecificCoupons.this, RedeemCouponConfirmed.class);

                                couponConfirmIntent.putExtra("name", intentName);
                                couponConfirmIntent.putExtra("username", intentUsername);
                                couponConfirmIntent.putExtra("age", intentAge);
                                couponConfirmIntent.putExtra("email", intentEmail);
                                couponConfirmIntent.putExtra("vendor", intentVendor);

                                LocationSpecificCoupons.this.startActivity(couponConfirmIntent);
                            }
                        };

                        RedeemCouponRequest redeemCouponRequest = new RedeemCouponRequest(coupon_id, username, responseListener);
                        RequestQueue queue = Volley.newRequestQueue(LocationSpecificCoupons.this);
                        queue.add(redeemCouponRequest);
                        */
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        siteRequest2.add(jsonVendorRequest);

        //----------------------------------------------------------------------------------------------------------------------------
    }
    private void configureVendorCouponButton() {
        ImageButton vendorCouponButton = (ImageButton) findViewById(R.id.managecoupon_btn);
        vendorCouponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent createCouponIntent = new Intent(CouponManage.this, CouponCreate.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                createCouponIntent.putExtra("company_name", company_name);
                createCouponIntent.putExtra("username", username);
                createCouponIntent.putExtra("email", email);


                Log.d("VCB", "company_name" + company_name);
                Log.d("VCB", "username" + username);
                Log.d("VCB", "email" + email);


                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                CouponManage.this.startActivity(createCouponIntent);
            }
        });
    }
    private void configureVendorSettingsButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.vendorsettings_btn);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent settingsIntent = new Intent(CouponManage.this, VendorSettings.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                settingsIntent.putExtra("company_name", company_name);
                settingsIntent.putExtra("username", username);
                settingsIntent.putExtra("email", email);


                Log.d("VSB", "company_name" + company_name);
                Log.d("VSB", "username" + username);
                Log.d("VSB", "email" + email);


                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                CouponManage.this.startActivity(settingsIntent);
            }
        });
    }

}

