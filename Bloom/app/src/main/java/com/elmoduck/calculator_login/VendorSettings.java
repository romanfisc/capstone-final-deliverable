package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

public class VendorSettings extends AppCompatActivity {

    String company_name;
    String username;
    String email;

    private Button logoutButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_settings);

        Intent settingsIntent = getIntent();
        String Cname = settingsIntent.getStringExtra("company_name");
        String Username = settingsIntent.getStringExtra("username");
        String Email = settingsIntent.getStringExtra("email");

        logoutButton = (Button) findViewById(R.id.logout_btn);

        company_name = Cname;
        username = Username;
        email = Email;

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VendorSettings.this, UserType.class);
                startActivity(intent);
            }
        });

        configureVendorHomeButton();
        configureVendorCouponButton();
    }

    private void configureVendorHomeButton() {
        ImageButton homeButton = (ImageButton) findViewById(R.id.vendorhome_btn);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent manageIntent = new Intent(VendorSettings.this, CouponManage.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                manageIntent.putExtra("company_name", company_name);
                manageIntent.putExtra("username", username);
                manageIntent.putExtra("email", email);


                Log.d("bsd", "company_name" + company_name);
                Log.d("bsd", "username" + username);
                Log.d("bsd", "email" + email);


                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                VendorSettings.this.startActivity(manageIntent);
            }
        });
    }

    private void configureVendorCouponButton() {
        ImageButton vendorCouponButton = (ImageButton) findViewById(R.id.managecoupon_btn);
        vendorCouponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent createCouponIntent = new Intent(VendorSettings.this, CouponCreate.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                createCouponIntent.putExtra("company_name", company_name);
                createCouponIntent.putExtra("username", username);
                createCouponIntent.putExtra("email", email);


                Log.d("bsd", "company_name" + company_name);
                Log.d("bsd", "username" + username);
                Log.d("bsd", "email" + email);


                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                VendorSettings.this.startActivity(createCouponIntent);
            }
        });
    }


}
