package com.elmoduck.calculator_login;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tyler G on 3/16/2018.
 */

public class CouponRequest extends StringRequest{
    private static final String REGISTER_REQUEST_URL = "http://cgi.soic.indiana.edu/~tygoblir/bloom/coupon_create.php";
    private Map<String, String> params;

    public CouponRequest (String username, String title, String description, int point_total, String coupon_code, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener ,null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("title", title);
        params.put("description", description);
        params.put("point_total", point_total + "");
        params.put("coupon_code", coupon_code);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
