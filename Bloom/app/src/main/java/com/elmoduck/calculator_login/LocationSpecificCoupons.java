package com.elmoduck.calculator_login;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LocationSpecificCoupons extends AppCompatActivity {

    RequestQueue siteRequest2;

    private static final String TAG = "MainActivity";

    List<String> couponList;//intentionally an empty string. Filled later
    List<String> couponDescriptionList;//intentionally an empty string. Filled later
    List<String> couponIDs;
    List<String> couponCodes;

    String intentVendorId;
    String intentVendor;

    //used for intents
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;
    String intentCouponCode;
    String intentCouponTitle;

    String phpUrl2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_specific_coupons);

        //This is where we activate the lists defined just a few lines ago
        couponList = new ArrayList<>();
        couponDescriptionList = new ArrayList<>();
        couponIDs = new ArrayList<>();
        couponCodes = new ArrayList<>();

        //Retrieve the intent that brought us here so that we can determine what data was passed along to this activity
        Intent locationCouponsIntent = getIntent();
        intentVendorId = locationCouponsIntent.getStringExtra("vendor_id");//pull the value of the vendor id, which was converted to a string in the previous java class
        intentVendor = locationCouponsIntent.getStringExtra("vendor");

        intentName = locationCouponsIntent.getStringExtra("name");
        intentUsername = locationCouponsIntent.getStringExtra("username");
        intentAge = locationCouponsIntent.getStringExtra("age");
        intentEmail = locationCouponsIntent.getStringExtra("email");

        phpUrl2 = "http://cgi.soic.indiana.edu/~eliduckw/bloom/bloom_request_vendor_coupons.php?vendorID="+intentVendorId;

        Log.d(TAG, "Build URL: " + phpUrl2);

        //This is just the header
        TextView mTitleWindow = findViewById(R.id.couponListHeader);
        mTitleWindow.setText(intentVendor);

        //This creates a new request for the database so that we can pull the location information
        siteRequest2 = Volley.newRequestQueue(this);

        Log.d(TAG, "We made it this far...");

        //------------------------------This is what pulls location and bio data from the database----------------------------------

        //phpUrl is defined at the top of this page
        JsonObjectRequest jsonVendorRequest = new JsonObjectRequest(Request.Method.GET, phpUrl2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Even though this is higher in this document than some code, the code below this section will be executed before this is done looping.
                //you're required to make this a try/catch when dealing with json requests
                try {
                    JSONArray jsonArray = response.getJSONArray("coupons");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject coupons = jsonArray.getJSONObject(i);
                        Log.d(TAG, "JSON array: " + coupons);

                        //These variables create strings unique to this iteration. They pull the data from the database by referencing the actual header in the database (eg "name" is a header in the database listing)
                        String pulledCouponID = coupons.getString("coupon_id");
                        String pulledCoupon = coupons.getString("title");
                        String pulledDescription = coupons.getString("description");
                        String pulledPoints = coupons.getString("point_total");
                        String pulledCodes = coupons.getString("coupon_code");

                        couponIDs.add(i, pulledCouponID);
                        couponList.add(i, pulledCoupon + " - " + pulledPoints + " points");
                        couponDescriptionList.add(i, pulledDescription);
                        couponCodes.add(i, pulledCodes);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //This logs to the debugger what our lists look like
                Log.d(TAG, "coupon list: " + couponList);
                Log.d(TAG, "description list: " + couponDescriptionList);
                Log.d(TAG, "Coupon ID list: " + couponIDs);

                //After we have generated the lists, we can assign the listLocations to the listView
                ListView lv = findViewById(R.id.listOfCoupons);//Here we create a variable to store the listView by its id. This way we can assign it a list in the next line
                lv.setAdapter(new ArrayAdapter<>(LocationSpecificCoupons.this, android.R.layout.simple_list_item_1, couponList));

                //This function makes the bio button clickable, taking you to the bio
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        final int couponPosition = position;//we make this because within this chunk is how we find the coupon position. this value is needed in the next onClick

                        intentCouponCode = couponCodes.get(position);
                        intentCouponTitle = couponList.get(position);

                        AlertDialog.Builder redeemAlert = new AlertDialog.Builder(LocationSpecificCoupons.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                        redeemAlert.setTitle("Bloom");
                        redeemAlert.setMessage("Are you sure you wish to redeem this coupon now? You will have a limited amount of time to use it.");
                        redeemAlert.setCancelable(false);

                        redeemAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final String username = intentUsername;
                                final String coupon_id = String.valueOf(couponIDs.get(couponPosition));

                                Response.Listener<String> responseListener = new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Intent couponConfirmIntent = new Intent(LocationSpecificCoupons.this, RedeemCouponConfirmed.class);

                                        couponConfirmIntent.putExtra("name", intentName);
                                        couponConfirmIntent.putExtra("username", intentUsername);
                                        couponConfirmIntent.putExtra("age", intentAge);
                                        couponConfirmIntent.putExtra("email", intentEmail);
                                        couponConfirmIntent.putExtra("vendor", intentVendor);
                                        couponConfirmIntent.putExtra("coupon_code", intentCouponCode);
                                        couponConfirmIntent.putExtra("coupon_title", intentCouponTitle);

                                        LocationSpecificCoupons.this.startActivity(couponConfirmIntent);
                                    }
                                };

                                RedeemCouponRequest redeemCouponRequest = new RedeemCouponRequest(coupon_id, username, responseListener);
                                RequestQueue queue = Volley.newRequestQueue(LocationSpecificCoupons.this);
                                queue.add(redeemCouponRequest);
                            }
                        });

                        redeemAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //does nothing
                            }
                        });

                        AlertDialog alert = redeemAlert.create();
                        alert.show();

                        /*
                        final String username = intentUsername;
                        final String coupon_id = String.valueOf(couponIDs.get(position));

                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Intent couponConfirmIntent = new Intent(LocationSpecificCoupons.this, RedeemCouponConfirmed.class);

                                couponConfirmIntent.putExtra("name", intentName);
                                couponConfirmIntent.putExtra("username", intentUsername);
                                couponConfirmIntent.putExtra("age", intentAge);
                                couponConfirmIntent.putExtra("email", intentEmail);
                                couponConfirmIntent.putExtra("vendor", intentVendor);

                                LocationSpecificCoupons.this.startActivity(couponConfirmIntent);
                            }
                        };

                        RedeemCouponRequest redeemCouponRequest = new RedeemCouponRequest(coupon_id, username, responseListener);
                        RequestQueue queue = Volley.newRequestQueue(LocationSpecificCoupons.this);
                        queue.add(redeemCouponRequest);
                        */
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        siteRequest2.add(jsonVendorRequest);

        //----------------------------------------------------------------------------------------------------------------------------
    }
}