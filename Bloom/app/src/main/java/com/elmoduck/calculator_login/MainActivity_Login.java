package com.elmoduck.calculator_login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity_Login extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private TextView Info;
    private int counter = 5;

    private LocationListener listener;

    //used for storing the user latitude and longitude. We update it later under ActivitiesNearMe but if ActivitiesNearMe is the first instance of GPS location tracking,
    //it can cause errors when trying to calculate distances as it will have a null value for a brief moment. By defining them here we prevent that null value from forming in the first place
    Double userLat;
    Double userLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__login);

        final TextView registerLink = (TextView) findViewById(R.id.tvRegisterHere);

        final EditText etUsername = (EditText)findViewById(R.id.etName);
        final EditText etPassword = (EditText)findViewById(R.id.etRegPassword);
        Info = (TextView)findViewById(R.id.tvInfo);
        final Button bLogin = (Button)findViewById(R.id.btnLogin);

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(MainActivity_Login.this, RegisterActivity.class);
                MainActivity_Login.this.startActivity(registerIntent);
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String name = jsonResponse.getString("name");
                                String username = jsonResponse.getString("username");
                                String age = jsonResponse.getString(String.valueOf("age"));
                                String email = jsonResponse.getString("email");
                                String points = jsonResponse.getString("points");

                                Log.d("bsd", "Login name: " + name);
                                Log.d("bsd", "Login username: " + username);
                                Log.d("bsd", "Login age: " + age);
                                Log.d("bsd", "Login email: " + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent activityIntent = new Intent(MainActivity_Login.this,  ActivitiesNearMe.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                activityIntent.putExtra("name", name);
                                activityIntent.putExtra("username", username);
                                activityIntent.putExtra("age", age);
                                activityIntent.putExtra("email", email);
                                activityIntent.putExtra("points", points);


                                //startActivity(intent);
                                MainActivity_Login.this.startActivity(activityIntent);

                            }else{
                                //counter for limiting login attempts
                                final Button buttonLogin = (Button)findViewById(R.id.btnLogin);
                                counter--;
                                Info.setText("Num of attempts remaining: "+ String.valueOf(counter));
                                if (counter == 0){
                                    buttonLogin.setEnabled(false);
                                }

                                //alert user that their attempt failed
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity_Login.this);
                                builder.setMessage("Login Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                LoginRequest loginRequest = new LoginRequest(username, password, responseListener);
                RequestQueue queue = Volley.newRequestQueue(MainActivity_Login.this);
                queue.add(loginRequest);
            }

        });

    }
}
