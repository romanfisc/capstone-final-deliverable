package com.elmoduck.calculator_login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class VendorRegister extends AppCompatActivity {

    //create variables for sending verification email
    private String editTextEmail;
    private String editTextSubject;
    private String editTextMessage;

    private LocationManager locationManager;
    private LocationListener listener;

    Double userLat;
    Double userLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_register);

        final EditText etVendorName = (EditText) findViewById(R.id.etVendorName);
        final EditText etVendorUsername = (EditText) findViewById(R.id.etVendorUsername);
        final EditText etVendorPassword = (EditText) findViewById(R.id.etVendorPassword);
        final EditText etVendorConfirmPassword = (EditText) findViewById(R.id.etVendorConfirmPassword);
        final EditText etVendorEmail = (EditText) findViewById(R.id.etVendorEmail);

        final Button vRegister = (Button) findViewById(R.id.vRegister);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                userLat = location.getLatitude();
                userLong = location.getLongitude();
                Log.d("message","current latitude: " + userLat);
                Log.d("message","current longitude: " + userLong);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET}
                        ,10);
            }
            return;
        }

        locationManager.requestLocationUpdates("gps", 2000, 0, listener);

        vRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name = etVendorName.getText().toString();
                final String username = etVendorUsername.getText().toString();
                final String password = etVendorPassword.getText().toString();
                final String confirmPassword = etVendorConfirmPassword.getText().toString();
                final String email = etVendorEmail.getText().toString();
                final String hash = username.hashCode() + "";
                final String active = "no";


                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        if (!password.equals(confirmPassword)) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(VendorRegister.this);
                            builder.setMessage("Your passwords do not match.")
                                    .setNegativeButton("Retry", null)
                                    .create()
                                    .show();
                        }else if (!(email.contains("@")) || !(email.contains("."))) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(VendorRegister.this);
                            builder.setMessage("You did not enter a valid email address.")
                                    .setNegativeButton("Retry", null)
                                    .create()
                                    .show();
                        }else {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                boolean usernameFine = jsonResponse.getBoolean("usernameFail");
                                boolean emailFine = jsonResponse.getBoolean("emailFail");

                                if (success) {
                                    //define the email variables so that the email can be formed
                                    editTextEmail = email;
                                    editTextSubject = "BLOOM - Verify your account";
                                    editTextMessage = "Hello, please follow the link to activate your Bloom account: http://cgi.soic.indiana.edu/~tygoblir/bloom/AuthenticateVendor.php?hash=" + String.valueOf(hash);

                                    //Getting content for email
                                    String regEmail = editTextEmail.trim();
                                    String subject = editTextSubject.trim();
                                    String message = editTextMessage.trim();

                                    //Creating SendMail object
                                    SendMail sm = new SendMail(VendorRegister.this, regEmail, subject, message);

                                    //Executing sendmail to send email
                                    sm.execute();

                                    //Register entries by user and return to login screen
                                    Intent intent = new Intent(VendorRegister.this, VendorLogin.class);
                                    VendorRegister.this.startActivity(intent);
                                } else if (usernameFine){
                                    //alert the user that the username they chose is already in use
                                    AlertDialog.Builder builder = new AlertDialog.Builder(VendorRegister.this);
                                    builder.setMessage("Sorry, that username is already taken.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                } else if (emailFine) {
                                    //alert the user that the email they entered is already associated with another account
                                    AlertDialog.Builder builder = new AlertDialog.Builder(VendorRegister.this);
                                    builder.setMessage("Sorry, that email is already associated with an account.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(VendorRegister.this);
                                    builder.setMessage("Sorry, there seems to be an error on our end.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };
                Log.d("message","current latitude1: " + userLat);
                Log.d("message","current longitude1: " + userLong);
                VendorRegisterRequest registerRequest = new VendorRegisterRequest(name, username, password, email, hash, userLat, userLong, active, responseListener);
                RequestQueue queue = Volley.newRequestQueue(VendorRegister.this);
                queue.add(registerRequest);
                Log.d("message","current latitude2: " + userLat);
                Log.d("message","current longitude2: " + userLong);;
            }
        });
    }
}
