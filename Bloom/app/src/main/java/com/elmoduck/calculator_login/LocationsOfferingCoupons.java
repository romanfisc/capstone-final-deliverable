package com.elmoduck.calculator_login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocationsOfferingCoupons extends AppCompatActivity {

    RequestQueue siteRequest;

    private static final String TAG = "MainActivity";

    //The lists we'll use to store locations and their bios
    //=====YOU MUST ACTIVATE THEM IN THE onCREATE FUNCTION!!!!!=====
    //List<Integer> vendorIDList;//intentionally an empty string. Filled later
    List<String> vendorList;//intentionally an empty string. Filled later
    List<Integer> vendorIDList;//intentionally an empty string. Filled later
    //List<String> bioList;//intentionally an empty string. Filled later
    List<Double> latitudeList;
    List<Double> longitudeList;
    List<String> milesFromLocations;
    List<String> vendorAndMiles;

    //stores location of the php script to get data from the database
    String phpUrl = "http://cgi.soic.indiana.edu/~eliduckw/bloom/bloom_request_vendors.php";

    //These two variables are used for gathering the latitude and longitude
    private LocationManager locationManager;
    private LocationListener listener;

    Double userLat;
    Double userLong;

    //used for intents
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    //used to check if we have listed the coupon locations yet
    Boolean couponLocationListed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_offering_coupons);



        //------This is just used as a simple loading indicator----------

        List<String> loading = Arrays.asList("Loading...");

        ListView lv = (ListView) findViewById(R.id.locationCouponList);//Here we create a variable to store the listView by its id. This way we can assign it a list in the next line
        lv.setAdapter(new ArrayAdapter<>(LocationsOfferingCoupons.this, android.R.layout.simple_list_item_1, loading));

        //---------------------------------------------------------------

        //retrieve the information sent to us
        Intent couponIntent = getIntent();
        String name = couponIntent.getStringExtra("name");
        String username = couponIntent.getStringExtra("username");
        String age = couponIntent.getStringExtra("age");
        String email = couponIntent.getStringExtra("email");

        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;

        //This is where we activate the lists defined just a few lines ago
        vendorList = new ArrayList<>();
        vendorIDList = new ArrayList<>();
        //bioList = new ArrayList<>();
        latitudeList = new ArrayList<>();
        longitudeList = new ArrayList<>();
        milesFromLocations = new ArrayList<>();
        vendorAndMiles = new ArrayList<>();

        couponLocationListed = false;

        //initiate buttons for navigation
        configureBioButton();
        configureActivityButton();
        configureSettingsButton();

        //This creates a new request for the database so that we can pull the location information
        siteRequest = Volley.newRequestQueue(this);

        //here we initialize one of the gps variables from earlier
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //--------------------------This is what calculates the current latitude and longitude of the user------------------------------

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                userLat = location.getLatitude();
                userLong = location.getLongitude();
                Log.d("msg", "latitude: " + userLat);
                Log.d("msg", "longitude: " + userLong);

                if (!couponLocationListed) {
                    populateLocationCouponView();
                    couponLocationListed = true;
                }

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };

        // first check for permissions (see if we are allowed to access the gps)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }

        //noinspection MissingPermission
        locationManager.requestLocationUpdates("gps", 3000, 0, listener);

        //--------------------------------------------------------------------------------------------------------------------------

    }
    //------------------------------This is what pulls location and bio data from the database----------------------------------

    private void populateLocationCouponView() {
        //phpUrl is defined at the top of this page
        JsonObjectRequest jsonVendorRequest = new JsonObjectRequest(Request.Method.GET, phpUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Even though this is higher in this document than some code, the code below this section will be executed before this is done looping.
                //you're required to make this a try/catch when dealing with json requests
                try {
                    JSONArray jsonArray = response.getJSONArray("vendors");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject vendors = jsonArray.getJSONObject(i);
                        //Log.d(TAG, "JSON array: " + vendors);

                        //These variables create strings unique to this iteration. They pull the data from the database by referencing the actual header in the database (eg "name" is a header in the database listing)
                        Integer pulledVendorID = vendors.getInt("vendor_id");
                        String pulledVendor = vendors.getString("company_name");
                        Double pulledLatitude = vendors.getDouble("latitude"); //!!!!!!!!!!!!!!!!!!!THESE ARE STILL NECESSARY!!!!!!!!!!! WE JUST COMMENTED THEM OUT BECAUSE SOMETIMES A VENDOR
                        Double pulledLongitude = vendors.getDouble("longitude");//!!!!!!!!!!!!!!!!!!WONT HAVE A LATITUDE, LONGITUDE, OR RADIUS AND WE WIND UP WITH ERRORS
                        //Double pulledRadius = vendors.getDouble("radius");


                        //This section calculates the distance in miles between the user's location and the target destination
                        double earthRadius = 3958.75; //the earth's radius in miles
                        double dLat = Math.toRadians(pulledLatitude - userLat);
                        double dLng = Math.toRadians(pulledLongitude - userLong);
                        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(pulledLatitude)) *
                                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
                        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                        float dist = (float) (earthRadius * c);

                        //This is how we round distances
                        double tempDist = Math.round(dist * 10);
                        double roundDist = tempDist / 10;

                        //each time a location and bio is found they are added to their respective lists
                        //vendorIDList.add(i, pulledVendorID);
                        vendorList.add(i, pulledVendor);
                        vendorIDList.add(i, pulledVendorID);
                        //bioList.add(i, pulledBio);
                        latitudeList.add(i, pulledLatitude);
                        longitudeList.add(i, pulledLongitude);
                        //milesFromLocations.add(i, String.valueOf(dist));
                        milesFromLocations.add(i, String.valueOf(roundDist));

                        vendorAndMiles.add(i, pulledVendor + " - " + String.valueOf(roundDist) + " miles away");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //This logs to the debugger what our lists look like
                //Log.d(TAG, "vendor list: " + vendorIDList);
                Log.d(TAG, "vendor list: " + vendorList);
                Log.d(TAG, "vendor ID list: " + vendorIDList);
                //Log.d(TAG, "bio list: " + bioList);
                //Log.d(TAG, "latitude list: " + latitudeList);
                //Log.d(TAG, "longitude list: " + longitudeList);
                Log.d(TAG, "miles from locations: " + milesFromLocations);

                //After we have generated the lists, we can assign the listLocations to the listView
                ListView lv = findViewById(R.id.locationCouponList);//Here we create a variable to store the listView by its id. This way we can assign it a list in the next line
                lv.setAdapter(new ArrayAdapter<>(LocationsOfferingCoupons.this, android.R.layout.simple_list_item_1, vendorAndMiles));

                //This function makes the bio button clickable, taking you to the bio
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent locationCouponsIntent = new Intent(LocationsOfferingCoupons.this, LocationSpecificCoupons.class);//Location page is where we are, BioPageFinal is the java class we are switching to.

                        //passes data along to Bio Page.
                        //Fun fact, the order of these putExtra's and the order of the code of the destination need to be the same
                        //(eg, here we send name then description because the bio page checks for name and then description
                        locationCouponsIntent.putExtra("vendor_id", String.valueOf(vendorIDList.get(position)));//you use .get(i) to get information from a list
                        locationCouponsIntent.putExtra("vendor", String.valueOf(vendorList.get(position)));
                        locationCouponsIntent.putExtra("name", intentName);
                        locationCouponsIntent.putExtra("username", intentUsername);
                        locationCouponsIntent.putExtra("age", intentAge);
                        locationCouponsIntent.putExtra("email", intentEmail);

                        startActivity(locationCouponsIntent);
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        siteRequest.add(jsonVendorRequest);

    }

    //----------------------------------------------------------------------------------------------------------------------------

    private void configureActivityButton() {
        ImageButton activitiesButton = (ImageButton) findViewById(R.id.activities_btn);
        activitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent activityIntent = new Intent(LocationsOfferingCoupons.this, ActivitiesNearMe.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                activityIntent.putExtra("name", intentName);
                activityIntent.putExtra("username", intentUsername);
                activityIntent.putExtra("age", intentAge);
                activityIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                LocationsOfferingCoupons.this.startActivity(activityIntent);
            }
        });
    }

    private void configureBioButton() {
        ImageButton bioButton = (ImageButton) findViewById(R.id.bio_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent settingsIntent = getIntent();

                final String name = settingsIntent.getStringExtra("name");
                final String username = settingsIntent.getStringExtra("username");
                final String age = settingsIntent.getStringExtra("age");
                final String email = settingsIntent.getStringExtra("email");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String points = jsonResponse.getString("total");



                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(LocationsOfferingCoupons.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                LocationsOfferingCoupons.this.startActivity(bioIntent);

                            }else{
                                String points = null;


                                Intent bioIntent = new Intent(LocationsOfferingCoupons.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                LocationsOfferingCoupons.this.startActivity(bioIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                BioPointRequest bioPointRequest = new BioPointRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(LocationsOfferingCoupons.this);
                queue.add(bioPointRequest);
            }

        });

    }

    private void configureSettingsButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.settings_btn);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent settingsIntent = new Intent(LocationsOfferingCoupons.this, UserSettings.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                settingsIntent.putExtra("name", intentName);
                settingsIntent.putExtra("username", intentUsername);
                settingsIntent.putExtra("age", intentAge);
                settingsIntent.putExtra("email", intentEmail);

                //Log.d("bsd", "Intent name: " + intentName);
                //Log.d("bsd", "Intent username: " + intentUsername);
                //Log.d("bsd", "Intent age: " + intentAge);
                //Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                LocationsOfferingCoupons.this.startActivity(settingsIntent);
            }
        });
    }

}