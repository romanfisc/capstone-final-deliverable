package com.elmoduck.calculator_login;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by Tyler G on 3/4/2018.
 */

public class VendorLoginRequest extends StringRequest{
    private static final String LOGIN_REQUEST_URL = "http://cgi.soic.indiana.edu/~tygoblir/bloom/vendor_login.php";
    private Map<String, String> params;

    public VendorLoginRequest(String username, String password, Response.Listener<String> listener){
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
