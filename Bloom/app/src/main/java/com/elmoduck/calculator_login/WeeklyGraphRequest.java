package com.elmoduck.calculator_login;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by Tyler G on 3/2/2018.
 */

public class WeeklyGraphRequest extends StringRequest{
    private static final String LOGIN_REQUEST_URL = "http://cgi.soic.indiana.edu/~tygoblir/bloom/graphWeekly.php";
    private Map<String, String> params;

    public WeeklyGraphRequest(String username, Response.Listener<String> listener){
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}