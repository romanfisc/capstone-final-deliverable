package com.elmoduck.calculator_login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import java.util.Locale;

public class RedeemCouponConfirmed extends AppCompatActivity {

    private static final long START_TIME_IN_MILLIS = 10000;

    TextView tvVendor;
    TextView tvCouponCode;
    TextView tvCouponTitle;
    TextView tvCouponTimer;

    Button btnReturnToVendors;

    String intentVendor;
    String intentCouponTitle;

    //used for intents
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    //used for timer
    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;
    private boolean mTimerEnded;
    private boolean activityChanged;
    private boolean timerHasEnded;

    private long mTimeLeftInMillis;
    private long mEndTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_coupon_confirmed);

        tvVendor = findViewById(R.id.couponConfirmationLocation);
        tvCouponCode = findViewById(R.id.couponConfirmationCode);
        tvCouponTitle = findViewById(R.id.couponConfirmationTitle);
        btnReturnToVendors = findViewById(R.id.couponConfirmationReturnBtn);
        tvCouponTimer = findViewById(R.id.couponConfirmationTimer);

        Intent locationCouponsIntent = getIntent();
        intentVendor = locationCouponsIntent.getStringExtra("vendor");
        intentCouponTitle = locationCouponsIntent.getStringExtra("coupon_title");

        tvCouponCode.setText(locationCouponsIntent.getStringExtra("coupon_code"));
        tvCouponTitle.setText(intentCouponTitle);

        intentName = locationCouponsIntent.getStringExtra("name");
        intentUsername = locationCouponsIntent.getStringExtra("username");
        intentAge = locationCouponsIntent.getStringExtra("age");
        intentEmail = locationCouponsIntent.getStringExtra("email");

        tvVendor.setText(intentVendor);

        mTimerEnded = false;
        activityChanged = true;
        timerHasEnded = false;

        startTimer();

        btnReturnToVendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent couponIntent = new Intent(RedeemCouponConfirmed.this, LocationsOfferingCoupons.class);

                couponIntent.putExtra("name", intentName);
                couponIntent.putExtra("username", intentUsername);
                couponIntent.putExtra("age", intentAge);
                couponIntent.putExtra("email", intentEmail);

                resetTimer();

                RedeemCouponConfirmed.this.startActivity(couponIntent);
                finish();

            }
        });


    }

    private void startTimer() {
        mEndTime = System.currentTimeMillis() + mTimeLeftInMillis;

        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
                activityChanged = false;
                mTimerEnded = false;
            }

            @Override
            public void onFinish() {
                if (!timerHasEnded) {
                    if (mTimerEnded == true) {
                        //updatePoints();
                        mTimerEnded = true;
                        returnToCoupons();
                    }
                    Log.d("message", "THIS TEXT SHOULD ONLY APPEAR ONCE");
                    resetTimer();
                }
            }
        }.start();

        mTimerRunning = true;
    }

    private void resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS;
        updateCountDownText();
        startTimer();
        returnToCoupons();
    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) Math.floor((mTimeLeftInMillis / 1000) % 60);

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        tvCouponTimer.setText(timeLeftFormatted);
    }

    private void returnToCoupons() {
        if (activityChanged == false) {
            activityChanged = true;
            timerHasEnded = true;
            Intent couponIntent = new Intent(RedeemCouponConfirmed.this, LocationsOfferingCoupons.class);

            couponIntent.putExtra("name", intentName);
            couponIntent.putExtra("username", intentUsername);
            couponIntent.putExtra("age", intentAge);
            couponIntent.putExtra("email", intentEmail);

            RedeemCouponConfirmed.this.startActivity(couponIntent);
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean("timerRunning", mTimerRunning);
        editor.putLong("endTime", mEndTime);

        editor.apply();

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);

        mTimerRunning = prefs.getBoolean("timerRunning", false);

        updateCountDownText();

        if (mTimerRunning) {
            mTimeLeftInMillis = mEndTime - System.currentTimeMillis();
            Log.d("message","Time Left In Millis: " + mTimeLeftInMillis);

            if (mTimeLeftInMillis < 0) {
                updateCountDownText();
                startTimer();
            } else {
                startTimer();
            }
        }
    }
}
