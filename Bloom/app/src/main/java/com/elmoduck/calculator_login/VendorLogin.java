package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class VendorLogin extends AppCompatActivity {

    private TextView Info;
    private int counter = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_login);

        final TextView registerLink = (TextView) findViewById(R.id.tvRegisterHere);

        final EditText etUsername = (EditText)findViewById(R.id.etName);
        final EditText etPassword = (EditText)findViewById(R.id.etRegPassword);
        Info = (TextView)findViewById(R.id.tvInfo);
        final Button bLogin = (Button)findViewById(R.id.btnLogin);

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(VendorLogin.this, VendorRegister.class);
                VendorLogin.this.startActivity(registerIntent);
            }
        });


        bLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String company_name = jsonResponse.getString("company_name");
                                String username = jsonResponse.getString("username");
                                String email = jsonResponse.getString("email");

                                //Log.d("asd", "name" + name);
                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent manageIntent = new Intent(VendorLogin.this, CouponManage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                manageIntent.putExtra("company_name", company_name);
                                manageIntent.putExtra("username", username);
                                manageIntent.putExtra("email", email);


                                //startActivity(intent);
                                VendorLogin.this.startActivity(manageIntent);

                            }else{
                                //counter for limiting login attempts
                                final Button buttonLogin = (Button)findViewById(R.id.btnLogin);
                                counter--;
                                Info.setText("Num of attempts remaining: "+ String.valueOf(counter));
                                if (counter == 0){
                                    buttonLogin.setEnabled(false);
                                }

                                //alert user that their attempt failed
                                AlertDialog.Builder builder = new AlertDialog.Builder(VendorLogin.this);
                                builder.setMessage("Login Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                VendorLoginRequest vendorloginRequest = new VendorLoginRequest(username, password, responseListener);
                RequestQueue queue = Volley.newRequestQueue(VendorLogin.this);
                queue.add(vendorloginRequest);
            }

        });

    }
}
