package com.elmoduck.calculator_login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import java.util.Locale;

public class PointEarningScreen extends AppCompatActivity {
    private static final long START_TIME_IN_MILLIS = 11000;

    private TextView mTextViewCountDown;
    private TextView mCountdownLocation;

    Button activitiesButton;

    //used for intents
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    Integer intentLocationId;
    Integer intentPoints;

    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;
    private boolean mTimerEnded;
    private boolean pointAdded;
    private boolean timerHasReset;

    private long mTimeLeftInMillis;
    private long mEndTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_earning_screen);

        mTextViewCountDown = findViewById(R.id.tvCountdown);
        mCountdownLocation = findViewById(R.id.tvCountdownLocation);
        activitiesButton = findViewById(R.id.btnCountdownReturn);

        pointAdded = true;
        mTimerEnded = false;
        timerHasReset = false;

        Intent pointEarningIntent = getIntent();

        Integer locationId = pointEarningIntent.getIntExtra("location_id", 0);
        String locationName = pointEarningIntent.getStringExtra("location_name");
        String name = pointEarningIntent.getStringExtra("name");
        Log.d("message", "--------THIS IS THE CURRENT USERS NAME--------------: " + name);
        String username = pointEarningIntent.getStringExtra("username");
        String age = pointEarningIntent.getStringExtra("age");
        String email = pointEarningIntent.getStringExtra("email");
        Integer points = pointEarningIntent.getIntExtra("points", 0);

        mCountdownLocation.setText(locationName);

        intentLocationId = locationId;
        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;
        intentPoints = points;

        startTimer();

        activitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent couponIntent = new Intent(PointEarningScreen.this, ActivitiesNearMe.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                couponIntent.putExtra("name", intentName);
                couponIntent.putExtra("username", intentUsername);
                couponIntent.putExtra("age", intentAge);
                couponIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                PointEarningScreen.this.startActivity(couponIntent);
            }
        });
    }


    private void startTimer() {
        mEndTime = System.currentTimeMillis() + mTimeLeftInMillis;

        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
                pointAdded = false;
                mTimerEnded = false;
                timerHasReset = false;

                /*
                int milliInt = (int) (Math.floor(millisUntilFinished / 1000) - 1);
                Log.d("message","##################CURRENT VALUE OF milliInt: " + milliInt);

                if (milliInt < 1) {
                    mTextViewCountDown.setText("00:00");
                }
                */
            }

            @Override
            public void onFinish() {
                if (mTimerEnded == false) {
                    updatePoints();
                    mTimerEnded = true;
                    resetTimer();
                }
                Log.d("message","THIS TEXT SHOULD ONLY APPEAR ONCE");
                //resetTimer();
            }
        }.start();

        mTimerRunning = true;
    }

    private void resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS;
        updateCountDownText();
        startTimer();
    }

    private void updatePoints() {
        if (pointAdded == false) {
            int points = intentPoints;
            int location_id = intentLocationId;
            final String username = intentUsername;
            Log.d("message","current user: " + intentUsername);

            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //From what I can tell nothing needs to be here. UpdateUserPoints just likes this being here to be the "listener", whatever that is 0_o
                }
            };
            UpdateUserPoints updateUserPoints = new UpdateUserPoints(username, points, location_id, responseListener);
            RequestQueue queue = Volley.newRequestQueue(PointEarningScreen.this);
            queue.add(updateUserPoints);
            pointAdded = true;
        }
    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (Math.floor((mTimeLeftInMillis / 1000) % 60)) - 1;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        mTextViewCountDown.setText(timeLeftFormatted);
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean("timerRunning", mTimerRunning);
        editor.putLong("endTime", mEndTime);

        editor.apply();

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);

        mTimerRunning = prefs.getBoolean("timerRunning", false);

        updateCountDownText();

        if (mTimerRunning) {
            mTimeLeftInMillis = mEndTime - System.currentTimeMillis();
            Log.d("message","Time Left In Millis: " + mTimeLeftInMillis);

            if (mTimeLeftInMillis < 0) {
                updateCountDownText();
                startTimer();
            } else {
                startTimer();
            }
        }
    }
}