package com.elmoduck.calculator_login;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tyler G on 4/15/2018.
 */

public class UpdateInfoRequest2 extends StringRequest {
    private static final String LOGIN_REQUEST_URL = "http://cgi.soic.indiana.edu/~tygoblir/bloom/update_info.php";
    private Map<String, String> params;

    public UpdateInfoRequest2(String username, String  input, String info, Response.Listener<String> listener){
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("input", input);
        params.put("info", info);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}