package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class EditInfo extends AppCompatActivity {

    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;
    String intentInfo;
    String intentPoints;
    String Name;
    String Username;
    String Age;
    String Email;
    String info;
    String input;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_info);

        final TextView realInfo = (TextView)findViewById(R.id.tvInfo);
        final EditText realHint = (EditText)findViewById(R.id.etInfoInput);


        Intent infoIntent = getIntent();

        String name1 = infoIntent.getStringExtra("name");
        String username1 = infoIntent.getStringExtra("username");
        String age1 = infoIntent.getStringExtra("age");
        String email1 = infoIntent.getStringExtra("email");
        String info1 = infoIntent.getStringExtra("info");
        String points = infoIntent.getStringExtra("points");

        //used for intents
        intentName = name1;
        intentUsername = username1;
        intentAge = age1;
        intentEmail = email1;
        intentInfo = info1;
        intentPoints = points;
        Name = "name";
        Username = "username";
        Age = "age";
        Email = "email";
        info = info1;


        configureBioButton();
        configureEditButton();

        realInfo.setText(intentInfo+":");

        Log.d("bsd", "info2: " + intentInfo);
        Log.d("bsd", "name1: " + intentName);
        Log.d("bsd", "name2: " + Name);

        if (intentInfo.equals(Name)) {
            realHint.setHint(intentName);
        }
        else if (intentInfo.equals(Username)) {
            realHint.setHint(intentUsername);
        }
        else if (intentInfo.equals(Age)) {
            realHint.setHint(intentAge);
        }
        else if (intentInfo.equals(Email)) {
            realHint.setHint(intentEmail);
        } else{
            realHint.setHint(intentInfo);
        }


        Log.d("bsd", "name: " + intentName);
        Log.d("bsd", "username: " + intentUsername);
        Log.d("bsd", "age: " + intentAge);
        Log.d("bsd", "email: " + intentEmail);
        Log.d("bsd", "info: " + intentInfo);
    }
    private void configureBioButton() {
        Button bioButton = (Button) findViewById(R.id.back_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent infoIntent = getIntent();

                final String name = infoIntent.getStringExtra("name");
                final String username = infoIntent.getStringExtra("username");
                final String age = infoIntent.getStringExtra("age");
                final String email = infoIntent.getStringExtra("email");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String points = jsonResponse.getString("total");



                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(EditInfo.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", intentPoints);



                                //startActivity(intent);
                                EditInfo.this.startActivity(bioIntent);

                            }else{
                                String points = null;


                                Intent bioIntent = new Intent(EditInfo.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", intentPoints);



                                //startActivity(intent);
                                EditInfo.this.startActivity(bioIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                BioPointRequest bioPointRequest = new BioPointRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(EditInfo.this);
                queue.add(bioPointRequest);
            }

        });
    }

    private void configureEditButton() {
        Button bioButton = (Button) findViewById(R.id.edit_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                final EditText realHint = (EditText)findViewById(R.id.etInfoInput);
                final String input = realHint.getText().toString();

                Log.d("asd", "name" + input);

                Intent infoIntent = getIntent();

                final String name = infoIntent.getStringExtra("name");
                final String username = infoIntent.getStringExtra("username");
                final String age = infoIntent.getStringExtra("age");
                final String email = infoIntent.getStringExtra("email");


                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean check = jsonResponse.getBoolean("check");

                            Log.d("message", "THIS IS THE CHECK: " + check);

                            if (check){
                                //this creates a new string variable that holds the user's name
                                if (intentInfo.equals(Name)) {
                                    intentName = input;
                                }
                                else if (intentInfo.equals(Username)) {
                                    intentUsername = input;
                                }
                                else if (intentInfo.equals(Age)) {
                                    intentAge = input;
                                }
                                else if (intentInfo.equals(Email)) {
                                    intentEmail = input;
                                }

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(EditInfo.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", intentPoints);

                                //startActivity(intent);
                                EditInfo.this.startActivity(bioIntent);

                                Log.d("inforequest2", "username" + username);
                                Log.d("inforequest2", "info" + info);
                                Log.d("inforequest2", "input" + input);

                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                //Intent bioIntent = new Intent(EditInfo.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                //bioIntent.putExtra("name", intentName);
                                //bioIntent.putExtra("username", intentUsername);
                                //bioIntent.putExtra("age", intentAge);
                                //bioIntent.putExtra("email", intentEmail);
                                //bioIntent.putExtra("points", points);

                            }else{
                                //alert the user that the username they chose is already in use
                                AlertDialog.Builder builder = new AlertDialog.Builder(EditInfo.this);
                                builder.setMessage("Sorry, that username is already taken.")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
                UpdateInfoRequest updateInfoRequest = new UpdateInfoRequest(username, input, info, responseListener);
                RequestQueue queue2 = Volley.newRequestQueue(EditInfo.this);
                queue2.add(updateInfoRequest);

                Log.d("inforequest", "username" + username);
                Log.d("inforequest", "info" + info);
                Log.d("inforequest", "input" + input);

            }

        });
    }
}