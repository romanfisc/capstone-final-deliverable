package com.elmoduck.calculator_login;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tyler G on 4/10/2018.
 */

public class DeactivateCouponRequest extends StringRequest {
    private static final String COUPON_REDEEM_REQUEST_URL = "http://cgi.soic.indiana.edu/~tygoblir/bloom/coupon_deactivate.php";
    private Map<String, String> params;

    public DeactivateCouponRequest (String coupon_id, String username, Response.Listener<String> listener){
        super(Method.POST, COUPON_REDEEM_REQUEST_URL, listener ,null);
        params = new HashMap<>();
        params.put("coupon_id", coupon_id);
        params.put("username", username);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
