package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class BioPage extends AppCompatActivity {

    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;
    String intentPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_page);
        final TextView realName = (TextView) findViewById(R.id.tvRealName);
        final TextView realUsername = (TextView) findViewById(R.id.tvRealUsername);
        final TextView realAge = (TextView) findViewById(R.id.tvRealAge);
        final TextView realEmail = (TextView) findViewById(R.id.tvRealEmail);
        final TextView realPoints = (TextView) findViewById(R.id.tvRealPoints);
        final Button bGraph = (Button)findViewById(R.id.graph_btn);

        Intent bioIntent = getIntent();

        String name = bioIntent.getStringExtra("name");
        String username = bioIntent.getStringExtra("username");
        String age = bioIntent.getStringExtra("age");
        String email = bioIntent.getStringExtra("email");
        String points = bioIntent.getStringExtra("points");
        //used for intents
        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;
        intentPoints = points;

        Log.d("bsd", "name: " + intentName);
        Log.d("bsd", "username: " + intentUsername);
        Log.d("bsd", "age: " + intentAge);
        Log.d("bsd", "email: " + intentEmail);

        realName.setText(name);
        realUsername.setText(username);
        realAge.setText(age);
        realEmail.setText(email);
        realPoints.setText(points);

        configureActivityButton();
        configureCouponButton();
        configureSettingsButton();

        configureEditNameButton();
        configureEditUsernameButton();
        configureEditAgeButton();
        configureEditEmailButton();

        bGraph.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent bioIntent = getIntent();

                final String username = bioIntent.getStringExtra("username");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String total = jsonResponse.getString("total");
                                String totalP = jsonResponse.getString("totalP");
                                String totalI = jsonResponse.getString("totalI");
                                String totalSp = jsonResponse.getString("totalSp");
                                String totalEn = jsonResponse.getString("totalEn");
                                String totalEm = jsonResponse.getString("totalEm");
                                String totalSo = jsonResponse.getString("totalSo");


                                Log.d("asd", "total" + total);
                                Log.d("asd", "totalP" + totalP);
                                Log.d("asd", "totalI" + totalI);
                                Log.d("asd", "totalSp" + totalSp);
                                Log.d("asd", "totalEn" + totalEn);
                                Log.d("asd", "totalEm" + totalEm);
                                Log.d("asd", "totalSo" + totalSo);


                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent graphIntent = new Intent(BioPage.this, Graph.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                graphIntent.putExtra("total", total);
                                graphIntent.putExtra("totalP", totalP);
                                graphIntent.putExtra("totalI", totalI);
                                graphIntent.putExtra("totalSp", totalSp);
                                graphIntent.putExtra("totalEn", totalEn);
                                graphIntent.putExtra("totalEm", totalEm);
                                graphIntent.putExtra("totalSo", totalSo);
                                graphIntent.putExtra("name", intentName);
                                graphIntent.putExtra("username", intentUsername);
                                graphIntent.putExtra("age", intentAge);
                                graphIntent.putExtra("email", intentEmail);



                                //startActivity(intent);
                                BioPage.this.startActivity(graphIntent);

                            }else{
                                String total = null;
                                String totalP = null;
                                String totalI = null;
                                String totalSp = null;
                                String totalEn = null;
                                String totalEm = null;
                                String totalSo = null;

                                Intent graphIntent = new Intent(BioPage.this, Graph.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                graphIntent.putExtra("total", total);
                                graphIntent.putExtra("totalP", totalP);
                                graphIntent.putExtra("totalI", totalI);
                                graphIntent.putExtra("totalSP", totalSp);
                                graphIntent.putExtra("totalEn", totalEn);
                                graphIntent.putExtra("totalEm", totalEm);
                                graphIntent.putExtra("totalSo", totalSo);
                                graphIntent.putExtra("name", intentName);
                                graphIntent.putExtra("username", intentUsername);
                                graphIntent.putExtra("age", intentAge);
                                graphIntent.putExtra("email", intentEmail);

                                Log.d("f", "total" + total);
                                Log.d("f", "totalP" + totalP);
                                Log.d("f", "totalI" + totalI);
                                Log.d("f", "totalSp" + totalSp);
                                Log.d("f", "totalEn" + totalEn);
                                Log.d("f", "totalEm" + totalEm);
                                Log.d("f", "totalSo" + totalSo);


                                //startActivity(intent);
                                BioPage.this.startActivity(graphIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                GraphRequest graphRequest = new GraphRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(BioPage.this);
                queue.add(graphRequest);
            }

        });

    }


    private void configureActivityButton() {
        ImageButton activitiesButton = (ImageButton) findViewById(R.id.activities_btn);
        activitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent activityIntent = new Intent(BioPage.this, ActivitiesNearMe.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                activityIntent.putExtra("name", intentName);
                activityIntent.putExtra("username", intentUsername);
                activityIntent.putExtra("age", intentAge);
                activityIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(activityIntent);
            }
        });
    }

    private void configureCouponButton() {
        ImageButton couponButton = (ImageButton) findViewById(R.id.coupon_btn);
        couponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent couponIntent = new Intent(BioPage.this, LocationsOfferingCoupons.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                couponIntent.putExtra("name", intentName);
                couponIntent.putExtra("username", intentUsername);
                couponIntent.putExtra("age", intentAge);
                couponIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(couponIntent);
            }
        });
    }


    private void configureSettingsButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.settings_btn);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent settingsIntent = new Intent(BioPage.this, UserSettings.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                settingsIntent.putExtra("name", intentName);
                settingsIntent.putExtra("username", intentUsername);
                settingsIntent.putExtra("age", intentAge);
                settingsIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(settingsIntent);
            }
        });
    }


    private void configureEditNameButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.editName);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String info = "name";
                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent editIntent = new Intent(BioPage.this, EditInfo.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                editIntent.putExtra("name", intentName);
                editIntent.putExtra("info", info);
                editIntent.putExtra("username", intentUsername);
                editIntent.putExtra("age", intentAge);
                editIntent.putExtra("email", intentEmail);
                editIntent.putExtra("points", intentPoints);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(editIntent);
            }
        });
    }
    private void configureEditUsernameButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.editUsername);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String info = "username";
                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent editIntent = new Intent(BioPage.this, EditInfo.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                editIntent.putExtra("name", intentName);
                editIntent.putExtra("info", info);
                editIntent.putExtra("username", intentUsername);
                editIntent.putExtra("age", intentAge);
                editIntent.putExtra("email", intentEmail);
                editIntent.putExtra("points", intentPoints);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(editIntent);
            }
        });
    }
    private void configureEditAgeButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.editAge);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String info = "age";
                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent editIntent = new Intent(BioPage.this, EditInfo2.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                editIntent.putExtra("name", intentName);
                editIntent.putExtra("info", info);
                editIntent.putExtra("username", intentUsername);
                editIntent.putExtra("age", intentAge);
                editIntent.putExtra("email", intentEmail);
                editIntent.putExtra("points", intentPoints);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(editIntent);
            }
        });
    }
    private void configureEditEmailButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.editEmail);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String info = "email";
                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent editIntent = new Intent(BioPage.this, EditInfo.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                editIntent.putExtra("name", intentName);
                editIntent.putExtra("info", info);
                editIntent.putExtra("username", intentUsername);
                editIntent.putExtra("age", intentAge);
                editIntent.putExtra("email", intentEmail);
                editIntent.putExtra("points", intentPoints);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                BioPage.this.startActivity(editIntent);
            }
        });
    }
}