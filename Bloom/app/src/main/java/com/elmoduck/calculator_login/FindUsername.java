package com.elmoduck.calculator_login;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Elijah on 11/21/2017.
 */

public class FindUsername extends StringRequest {
    private static final String USERNAME_REQUEST_URL = "http://cgi.soic.indiana.edu/~eliduckw/bloom/findUsername.php";
    private Map<String, String> params;

    public FindUsername(String username, Response.Listener<String> listener){
        super(Method.POST, USERNAME_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
