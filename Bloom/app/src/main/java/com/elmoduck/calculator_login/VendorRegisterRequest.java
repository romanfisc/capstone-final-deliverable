package com.elmoduck.calculator_login;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by Tyler G on 3/4/2018.
 */

public class VendorRegisterRequest extends StringRequest{
    private static final String REGISTER_REQUEST_URL = "http://cgi.soic.indiana.edu/~tygoblir/bloom/vendor_register.php";
    private Map<String, String> params;

    public VendorRegisterRequest(String name, String username, String password, String email, String hash, Double userLat, Double userLong, String active, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("name", name);
        params.put("username", username);
        params.put("password", password);
        params.put("email", email);
        params.put("hash", hash);
        params.put("userLat", userLat + "");
        params.put("userLong", userLong + "");
        params.put("active", active);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}