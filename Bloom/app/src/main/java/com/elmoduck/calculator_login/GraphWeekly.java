package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class GraphWeekly extends AppCompatActivity {
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_weekly);

        final TextView totalW1 = (TextView) findViewById(R.id.tvTotalW);
        final TextView totalPW1 = (TextView) findViewById(R.id.tvPhysicalW);
        final TextView totalIW1 = (TextView) findViewById(R.id.tvIntellectualW);
        final TextView totalSpW1 = (TextView) findViewById(R.id.tvSpiritualW);
        final TextView totalEnW1 = (TextView) findViewById(R.id.tvEnvironmentalW);
        final TextView totalEmW1 = (TextView) findViewById(R.id.tvEmotionalW);
        final TextView totalSoW1 = (TextView) findViewById(R.id.tvSocialW);
        final Button bGraph = (Button)findViewById(R.id.totalGraph_btn);

        Intent graphWIntent = getIntent();

        String name = graphWIntent.getStringExtra("name");
        String username = graphWIntent.getStringExtra("username");
        String age = graphWIntent.getStringExtra("age");
        String email = graphWIntent.getStringExtra("email");
        String totalW = graphWIntent.getStringExtra("totalW");
        String totalPW = graphWIntent.getStringExtra("totalPW");
        String totalIW = graphWIntent.getStringExtra("totalIW");
        String totalSpW = graphWIntent.getStringExtra("totalSpW");
        String totalEnW = graphWIntent.getStringExtra("totalEnW");
        String totalEmW = graphWIntent.getStringExtra("totalEmW");
        String totalSoW = graphWIntent.getStringExtra("totalSoW");

        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;

        Log.d("w", "total" + totalW);
        Log.d("w", "totalP" + totalPW);
        Log.d("w", "totalI" + totalIW);
        Log.d("w", "totalSp" + totalSpW);
        Log.d("w", "totalEn" + totalEnW);
        Log.d("w", "totalEm" + totalEmW);
        Log.d("w", "totalSo" + totalSoW);

        totalW1.setText(totalW);
        totalPW1.setText(totalPW);
        totalIW1.setText(totalIW);
        totalSpW1.setText(totalSpW);
        totalEnW1.setText(totalEnW);
        totalEmW1.setText(totalEmW);
        totalSoW1.setText(totalSoW);

        configureBioButton();

        bGraph.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent bioIntent = getIntent();

                final String username = bioIntent.getStringExtra("username");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String total = jsonResponse.getString("total");
                                String totalP = jsonResponse.getString("totalP");
                                String totalI = jsonResponse.getString("totalI");
                                String totalSp = jsonResponse.getString("totalSp");
                                String totalEn = jsonResponse.getString("totalEn");
                                String totalEm = jsonResponse.getString("totalEm");
                                String totalSo = jsonResponse.getString("totalSo");


                                Log.d("asd", "total" + total);
                                Log.d("asd", "totalP" + totalP);
                                Log.d("asd", "totalI" + totalI);
                                Log.d("asd", "totalSp" + totalSp);
                                Log.d("asd", "totalEn" + totalEn);
                                Log.d("asd", "totalEm" + totalEm);
                                Log.d("asd", "totalSo" + totalSo);


                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent graphIntent = new Intent(GraphWeekly.this, Graph.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                graphIntent.putExtra("total", total);
                                graphIntent.putExtra("totalP", totalP);
                                graphIntent.putExtra("totalI", totalI);
                                graphIntent.putExtra("totalSp", totalSp);
                                graphIntent.putExtra("totalEn", totalEn);
                                graphIntent.putExtra("totalEm", totalEm);
                                graphIntent.putExtra("totalSo", totalSo);
                                graphIntent.putExtra("name", intentName);
                                graphIntent.putExtra("username", intentUsername);
                                graphIntent.putExtra("age", intentAge);
                                graphIntent.putExtra("email", intentEmail);



                                //startActivity(intent);
                                GraphWeekly.this.startActivity(graphIntent);

                            }else{
                                String total = null;
                                String totalP = null;
                                String totalI = null;
                                String totalSp = null;
                                String totalEn = null;
                                String totalEm = null;
                                String totalSo = null;

                                Intent graphIntent = new Intent(GraphWeekly.this, Graph.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                graphIntent.putExtra("total", total);
                                graphIntent.putExtra("totalP", totalP);
                                graphIntent.putExtra("totalI", totalI);
                                graphIntent.putExtra("totalSP", totalSp);
                                graphIntent.putExtra("totalEn", totalEn);
                                graphIntent.putExtra("totalEm", totalEm);
                                graphIntent.putExtra("totalSo", totalSo);
                                graphIntent.putExtra("name", intentName);
                                graphIntent.putExtra("username", intentUsername);
                                graphIntent.putExtra("age", intentAge);
                                graphIntent.putExtra("email", intentEmail);

                                Log.d("f", "total" + total);
                                Log.d("f", "totalP" + totalP);
                                Log.d("f", "totalI" + totalI);
                                Log.d("f", "totalSp" + totalSp);
                                Log.d("f", "totalEn" + totalEn);
                                Log.d("f", "totalEm" + totalEm);
                                Log.d("f", "totalSo" + totalSo);


                                //startActivity(intent);
                                GraphWeekly.this.startActivity(graphIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                GraphRequest graphRequest = new GraphRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(GraphWeekly.this);
                queue.add(graphRequest);
            }

        });

    }
    private void configureBioButton() {
        Button bioButton = (Button) findViewById(R.id.bio_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent graphWIntent = getIntent();

                final String name = graphWIntent.getStringExtra("name");
                final String username = graphWIntent.getStringExtra("username");
                final String age = graphWIntent.getStringExtra("age");
                final String email = graphWIntent.getStringExtra("email");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String points = jsonResponse.getString("total");



                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(GraphWeekly.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                GraphWeekly.this.startActivity(bioIntent);

                            }else{
                                String points = null;


                                Intent bioIntent = new Intent(GraphWeekly.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                GraphWeekly.this.startActivity(bioIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                BioPointRequest bioPointRequest = new BioPointRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(GraphWeekly.this);
                queue.add(bioPointRequest);
            }

        });
    }
}