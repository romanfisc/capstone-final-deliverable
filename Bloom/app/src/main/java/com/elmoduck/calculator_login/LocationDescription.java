package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class LocationDescription extends AppCompatActivity {

    //used for intents
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    Integer intentLocationId;
    Double intentRadius;
    Float intentDistance;
    Integer intentPoints;

    String intentLocation;

    //This is just the button that takes us to the point earning screen
    Button btnEarnPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_description);
        TextView mLocationWindow = (TextView) findViewById(R.id.locationBio);
        TextView mPointWindow = (TextView) findViewById(R.id.locationPoints);

        ImageView mLocationPic = findViewById(R.id.locationActivityImage);

        btnEarnPoints = findViewById(R.id.btnPointEarningScreen);

        //Retrieve the intent that brought us here so that we can determine what data was passed along to this activity
        Intent locationBioIntent = getIntent();

        Integer location_id = locationBioIntent.getIntExtra("location_id", 0);
        String name = locationBioIntent.getStringExtra("name");
        String username = locationBioIntent.getStringExtra("username");
        String age = locationBioIntent.getStringExtra("age");
        String email = locationBioIntent.getStringExtra("email");
        Double radius = locationBioIntent.getDoubleExtra("radius", 0);
        Integer points = locationBioIntent.getIntExtra("points", 0);
        String photo = locationBioIntent.getStringExtra("photo");

        //This is how we display photos on the screen
        Picasso.get().load(photo).into(mLocationPic);

        //http://cgi.soic.indiana.edu/~eliduckw/bloom/photos/NameOfImage.png -- This is just a reference for in case we have to add more photos later

        intentLocationId = location_id;
        Log.d("message", "FIRST LOG OF LOCATION DESCRIPTION, LOCATION_ID = " + intentLocationId);
        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;
        intentRadius = radius;
        intentPoints = points;

        //Sets the text for the header on the screen
        String mLocationName = locationBioIntent.getStringExtra("location_name");//This will hold the location stored in the database. Also a great example of why we retrieved the intent
        intentLocation = mLocationName;
        Log.d("message", "-----THIS IS WHAT THE INTENT LOCATION IS-----: " + mLocationName);//currently debugging why users name is location bio
        Float mLocationDistance = locationBioIntent.getFloatExtra("milesFromLocation", 0);//This will hold the distance of the current location from the user
        intentDistance = mLocationDistance;

        //This is how we round distances
        double tempDist = Math.round(mLocationDistance * 100);
        double roundDist = tempDist / 100;

        //This is the variable used to store the words that form the bio
        String bioDescription = locationBioIntent.getStringExtra("description");
        mLocationWindow.setText(bioDescription + " - " + roundDist + " miles away.");//must use toString as stringBuilder technically doesn't generate a string on its own

        //This lets the user know how many points they will earn while doing an activity:
        String activityPoints = "Points Earned Per Minute: " + Integer.toString(intentPoints);
        mPointWindow.setText(activityPoints);

        btnEarnPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intentDistance < intentRadius) {
                    Intent pointEarningIntent = new Intent(LocationDescription.this, PointEarningScreen.class);//Location page is where we are, PointEarningScreen is the java class we are switching to.

                    pointEarningIntent.putExtra("location_name", intentLocation);

                    pointEarningIntent.putExtra("location_id", intentLocationId);
                    Log.d("message", "SECOND LOG ON LOCATION DESCRIPTION, LOCATION_ID = " + intentLocationId);
                    pointEarningIntent.putExtra("name", intentName);
                    pointEarningIntent.putExtra("username", intentUsername);
                    pointEarningIntent.putExtra("age", intentAge);
                    pointEarningIntent.putExtra("email", intentEmail);
                    pointEarningIntent.putExtra("points", intentPoints);

                    Log.d("message", "#########################################Location ID being passed over: " + intentLocationId);
                    Log.d("message", "#########################################Name being passed over: " + intentName);
                    Log.d("message", "#########################################Username being passed over: " + intentUsername);
                    Log.d("message", "#########################################Age being passed over: " + intentAge);
                    Log.d("message", "#########################################Email being passed over: " + intentEmail);
                    Log.d("message", "#########################################Points being passed over: " + intentPoints);

                    startActivity(pointEarningIntent);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LocationDescription.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                    builder.setMessage("Sorry, you are not close enough to that activity.")
                            .setNegativeButton("Okay", null)
                            .create()
                            .show();
                }
            }
        });
    }
}