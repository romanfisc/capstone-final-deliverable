package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class UserSettings extends AppCompatActivity {

    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    private Button logoutButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        logoutButton = (Button) findViewById(R.id.logout_btn);

        Intent settingsIntent = getIntent();

        String name = settingsIntent.getStringExtra("name");
        String username = settingsIntent.getStringExtra("username");
        String age = settingsIntent.getStringExtra("age");
        String email = settingsIntent.getStringExtra("email");

        //used for intents
        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserSettings.this, UserType.class);
                startActivity(intent);
            }
        });
        configureActivityButton();
        configureCouponButton();
        configureBioButton();
    }

    private void configureActivityButton() {
        ImageButton activitiesButton = (ImageButton) findViewById(R.id.activities_btn);
        activitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent activityIntent = new Intent(UserSettings.this, ActivitiesNearMe.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                activityIntent.putExtra("name", intentName);
                activityIntent.putExtra("username", intentUsername);
                activityIntent.putExtra("age", intentAge);
                activityIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                UserSettings.this.startActivity(activityIntent);
            }
        });
    }

    private void configureCouponButton() {
        ImageButton couponButton = (ImageButton) findViewById(R.id.coupon_btn);
        couponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent couponIntent = new Intent(UserSettings.this, LocationsOfferingCoupons.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                couponIntent.putExtra("name", intentName);
                couponIntent.putExtra("username", intentUsername);
                couponIntent.putExtra("age", intentAge);
                couponIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                UserSettings.this.startActivity(couponIntent);
            }
        });
    }
    private void configureBioButton() {
        ImageButton bioButton = (ImageButton) findViewById(R.id.bio_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent settingsIntent = getIntent();

                final String name = settingsIntent.getStringExtra("name");
                final String username = settingsIntent.getStringExtra("username");
                final String age = settingsIntent.getStringExtra("age");
                final String email = settingsIntent.getStringExtra("email");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String points = jsonResponse.getString("total");



                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(UserSettings.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                UserSettings.this.startActivity(bioIntent);

                            }else{
                                String points = null;


                                Intent bioIntent = new Intent(UserSettings.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                UserSettings.this.startActivity(bioIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                BioPointRequest bioPointRequest = new BioPointRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(UserSettings.this);
                queue.add(bioPointRequest);
            }

        });

    }

}
