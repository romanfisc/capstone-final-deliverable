package com.elmoduck.calculator_login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItem;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivitiesNearMe extends AppCompatActivity {

    RequestQueue siteRequest;

    private static final String TAG = "MainActivity";

    //The lists we'll use to store locations and their bios
    //=====YOU MUST ACTIVATE THEM IN THE onCREATE FUNCTION!!!!!=====
    List<Integer> locationIdList;//intentionally an empty string. Filled later
    List<String> locationList;//intentionally an empty string. Filled later
    List<String> bioList;//intentionally an empty string. Filled later
    List<Double> latitudeList;
    List<Double> longitudeList;
    List<Double> radiusList;
    List<Float> milesFromLocations;
    List<Integer> pointsList;
    List<String> photoList;//photos

    //stores location of the php script to get data from the database
    String phpUrl = "http://cgi.soic.indiana.edu/~eliduckw/bloom/bloom_request_locations.php";

    //These two variables are used for gathering the latitude and longitude
    private LocationManager locationManager;
    private LocationListener listener;

    Double userLat;
    Double userLong;

    //These next two are just for debugging distances
    Double anytimeLat;
    Double anytimeLong;

    //used for intents
    Integer intentLocationId;
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;
    String intentUserLat;
    String intentUserLong;
    Double intentLocationRadius;
    Integer intentPoints;

    Boolean activitiesListed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activities_near_me);

        //------This is just used as a simple loading indicator----------

        List<String> loading = Arrays.asList("Loading...");

        ListView lv = (ListView) findViewById(R.id.listLocations);//Here we create a variable to store the listView by its id. This way we can assign it a list in the next line
        lv.setAdapter(new ArrayAdapter<>(ActivitiesNearMe.this, android.R.layout.simple_list_item_1, loading));

        //---------------------------------------------------------------

        Intent activityIntent = getIntent();
        String name = activityIntent.getStringExtra("name");
        String username = activityIntent.getStringExtra("username");
        String age = activityIntent.getStringExtra("age");
        String email = activityIntent.getStringExtra("email");
        String user_lat = activityIntent.getStringExtra("user_lat");
        String user_long = activityIntent.getStringExtra("user_long");

        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;
        intentUserLat = user_lat;
        intentUserLong = user_long;

        activitiesListed = false;


        Log.d("bsd", "activities name: " + name);
        Log.d("bsd", "activities username: " + username);
        Log.d("bsd", "activities age: " + age);
        Log.d("bsd", "activities email: " + email);

        //This is where we activate the lists defined just a few lines ago
        locationIdList = new ArrayList<>();
        locationList = new ArrayList<>();
        bioList = new ArrayList<>();
        latitudeList = new ArrayList<>();
        longitudeList = new ArrayList<>();
        radiusList = new ArrayList<>();
        milesFromLocations = new ArrayList<>();
        pointsList = new ArrayList<>();
        photoList = new ArrayList<>();

        //This creates a new request for the database so that we can pull the location information
        siteRequest = Volley.newRequestQueue(this);

        //here we initialize one of the gps variables from earlier
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Allows the button which switches us to the user bio to function
        configureBioButton();
        configureCouponButton();
        configureSettingsButton();


        //--------------------------This is what calculates the current latitude and longitude of the user------------------------------

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                userLat = location.getLatitude();
                userLong = location.getLongitude();

                if (activitiesListed == false) {
                    populateListView();
                    activitiesListed = true;
                }

                Log.d(TAG,"current latitude: " + userLat);
                Log.d(TAG,"current longitude: " + userLong);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };

        // first check for permissions (see if we are allowed to access the gps)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET}
                        ,10);
            }
            return;
        }

        //Every 5 seconds refresh the userLat and userLong so that our location is up-to-date
        locationManager.requestLocationUpdates("gps", 2000, 0, listener);

        //--------------------------------------------------------------------------------------------------------------------------

        //This is still onCreate

    }

    private void populateListView() {
        //------------------------------This is what pulls location and bio data from the database----------------------------------

        //phpUrl is defined at the top of this page
        JsonObjectRequest jsonLocationRequest = new JsonObjectRequest(Request.Method.GET, phpUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Even though this is higher in this document than some code, the code below this section will be executed before this is done looping.
                //you're required to make this a try/catch when dealing with json requests
                try {
                    JSONArray jsonArray = response.getJSONArray("locations");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject location = jsonArray.getJSONObject(i);

                        //These variables create strings unique to this iteration. They pull the data from the database by referencing the actual header in the database (eg "name" is a header in the database listing)
                        Integer pulledLocationId = location.getInt("location_id");
                        String pulledLocation = location.getString("name");
                        String pulledBio = location.getString("description");
                        Double pulledLatitude = location.getDouble("latitude");
                        Double pulledLongitude = location.getDouble("longitude");
                        Double pulledRadius = location.getDouble("radius");
                        Integer pulledPoints = location.getInt("points");
                        String pulledPhoto = location.getString("photo");


                        //This section calculates the distance in miles between the user's location and the target destination
                        double earthRadius = 3958.75; //the earth's radius in miles
                        double dLat = Math.toRadians(pulledLatitude-userLat);
                        double dLng = Math.toRadians(pulledLongitude-userLong);
                        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                                Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(pulledLatitude)) *
                                        Math.sin(dLng/2) * Math.sin(dLng/2);
                        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                        float dist = (float) (earthRadius * c);


                        //each time a location and bio is found they are added to their respective lists
                        locationIdList.add(i, pulledLocationId);
                        locationList.add(i, pulledLocation);
                        bioList.add(i, pulledBio);
                        latitudeList.add(i, pulledLatitude);
                        longitudeList.add(i, pulledLongitude);
                        radiusList.add(i, pulledRadius);
                        milesFromLocations.add(i, dist);
                        pointsList.add(i, pulledPoints);
                        photoList.add(i, pulledPhoto);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //This logs to the debugger what our lists look like
                Log.d(TAG, "location ID list: " + locationIdList);
                Log.d(TAG, "location list: " + locationList);
                Log.d(TAG, "bio list: " + bioList);
                //Log.d(TAG, "latitude list: " + latitudeList);
                //Log.d(TAG, "longitude list: " + longitudeList);
                Log.d(TAG, "miles from locations: " + milesFromLocations);
                Log.d(TAG, "points list: " + pointsList);

                //After we have generated the lists, we can assign the listLocations to the listView
                ListView lv = (ListView) findViewById(R.id.listLocations);//Here we create a variable to store the listView by its id. This way we can assign it a list in the next line
                lv.setAdapter(new ArrayAdapter<>(ActivitiesNearMe.this, android.R.layout.simple_list_item_1, locationList));

                //This function makes the activity bio button clickable, taking you to the activity bio
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent locationBioIntent = new Intent(ActivitiesNearMe.this, LocationDescription.class);//Location page is where we are, BioPageFinal is the java class we are switching to.

                        intentLocationRadius = radiusList.get(position);
                        Log.d("message", "=======================CURRENT LOCATION RADIUS====================: " + intentLocationRadius);
                        intentLocationId = locationIdList.get(position);
                        Log.d("message", "=========LOCATION ID BEING PASSED TO LOCATION DESC==========: " + intentLocationId);
                        intentPoints = pointsList.get(position);

                        //passes data along to Bio Page.
                        //Fun fact, the order of these putExtra's and the order of the code of the destination need to be the same
                        //(eg, here we send name then description because the bio page checks for name and then description
                        locationBioIntent.putExtra("location_name", locationList.get(position));//you use .get(i) to get information from a list
                        locationBioIntent.putExtra("milesFromLocation", milesFromLocations.get(position));
                        locationBioIntent.putExtra("description", bioList.get(position));
                        locationBioIntent.putExtra("photo", photoList.get(position));

                        locationBioIntent.putExtra("location_id", intentLocationId);
                        locationBioIntent.putExtra("name", intentName);
                        locationBioIntent.putExtra("username", intentUsername);
                        locationBioIntent.putExtra("age", intentAge);
                        locationBioIntent.putExtra("email", intentEmail);
                        locationBioIntent.putExtra("radius", intentLocationRadius);
                        locationBioIntent.putExtra("points", intentPoints);

                        startActivity(locationBioIntent);
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        siteRequest.add(jsonLocationRequest);

        //----------------------------------------------------------------------------------------------------------------------------
    }


    private void configureBioButton() {
        ImageButton bioButton = (ImageButton) findViewById(R.id.bio_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent settingsIntent = getIntent();

                final String name = settingsIntent.getStringExtra("name");
                final String username = settingsIntent.getStringExtra("username");
                final String age = settingsIntent.getStringExtra("age");
                final String email = settingsIntent.getStringExtra("email");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String points = jsonResponse.getString("total");



                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(ActivitiesNearMe.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                ActivitiesNearMe.this.startActivity(bioIntent);

                            }else{
                                String points = null;


                                Intent bioIntent = new Intent(ActivitiesNearMe.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                ActivitiesNearMe.this.startActivity(bioIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                BioPointRequest bioPointRequest = new BioPointRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(ActivitiesNearMe.this);
                queue.add(bioPointRequest);
            }

        });

    }

    private void configureCouponButton() {
        ImageButton couponButton = (ImageButton) findViewById(R.id.coupon_btn);
        couponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent couponIntent = new Intent(ActivitiesNearMe.this, LocationsOfferingCoupons.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                couponIntent.putExtra("name", intentName);
                couponIntent.putExtra("username", intentUsername);
                couponIntent.putExtra("age", intentAge);
                couponIntent.putExtra("email", intentEmail);

                Log.d("bsd", "Intent name: " + intentName);
                Log.d("bsd", "Intent username: " + intentUsername);
                Log.d("bsd", "Intent age: " + intentAge);
                Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                ActivitiesNearMe.this.startActivity(couponIntent);
            }
        });
    }

    private void configureSettingsButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.settings_btn);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent settingsIntent = new Intent(ActivitiesNearMe.this, UserSettings.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                settingsIntent.putExtra("name", intentName);
                settingsIntent.putExtra("username", intentUsername);
                settingsIntent.putExtra("age", intentAge);
                settingsIntent.putExtra("email", intentEmail);

                //Log.d("bsd", "Intent name: " + intentName);
                //Log.d("bsd", "Intent username: " + intentUsername);
                //Log.d("bsd", "Intent age: " + intentAge);
                //Log.d("bsd", "Intent email: " + intentEmail);

                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                ActivitiesNearMe.this.startActivity(settingsIntent);
            }
        });
    }

}