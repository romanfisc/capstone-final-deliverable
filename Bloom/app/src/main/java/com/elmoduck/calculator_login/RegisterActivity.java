package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    //create variables for sending verification email
    private String editTextEmail;
    private String editTextSubject;
    private String editTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText etRegName = (EditText) findViewById(R.id.etRegName);
        final EditText etRegUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etRegPassword = (EditText) findViewById(R.id.etRegPassword);
        final EditText etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        final EditText etAge = (EditText) findViewById(R.id.etAge);
        final EditText etRegEmail = (EditText) findViewById(R.id.etRegEmail);

        final Button bRegister = (Button) findViewById(R.id.bRegister);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name = etRegName.getText().toString();
                final String username = etRegUsername.getText().toString();
                final int age = Integer.parseInt(etAge.getText().toString());
                final String password = etRegPassword.getText().toString();
                final String confirmPassword = etConfirmPassword.getText().toString();
                final String email = etRegEmail.getText().toString();
                final String hash = username.hashCode() + "";
                final String active = "no";

                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        if (!password.equals(confirmPassword)) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setMessage("Your passwords do not match.")
                                    .setNegativeButton("Retry", null)
                                    .create()
                                    .show();
                        }else if (!(email.contains("@")) || !(email.contains("."))) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage("You did not enter a valid email address.")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                        }else {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                boolean usernameFine = jsonResponse.getBoolean("usernameFail");
                                boolean emailFine = jsonResponse.getBoolean("emailFail");

                                if (success) {
                                    //define the email variables so that the email can be formed
                                    editTextEmail = email;
                                    editTextSubject = "BLOOM - Verify your account";
                                    editTextMessage = "Hello, please follow the link to activate your Bloom account: http://cgi.soic.indiana.edu/~tygoblir/bloom/Authenticate.php?hash=" + String.valueOf(hash);

                                    //Getting content for email
                                    String regEmail = editTextEmail.trim();
                                    String subject = editTextSubject.trim();
                                    String message = editTextMessage.trim();

                                    //Creating SendMail object
                                    SendMail sm = new SendMail(RegisterActivity.this, regEmail, subject, message);

                                    //Executing sendmail to send email
                                    sm.execute();

                                    //Register entries by user and return to login screen
                                    Intent intent = new Intent(RegisterActivity.this, MainActivity_Login.class);
                                    RegisterActivity.this.startActivity(intent);
                                } else if (usernameFine){
                                    //alert the user that the username they chose is already in use
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage("Sorry, that username is already taken.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                } else if (emailFine) {
                                    //alert the user that the email they entered is already associated with another account
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage("Sorry, that email is already associated with an account.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage("Sorry, there seems to be an error on our end.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };

                RegisterRequest registerRequest = new RegisterRequest(name, username, age, password, email, hash, active, responseListener);
                RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                queue.add(registerRequest);
            }
        });
    }
}
