package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Graph extends AppCompatActivity {
    String intentName;
    String intentUsername;
    String intentAge;
    String intentEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        final TextView total1 = (TextView) findViewById(R.id.tvTotal);
        final TextView totalP1 = (TextView) findViewById(R.id.tvPhysical);
        final TextView totalI1 = (TextView) findViewById(R.id.tvIntellectual);
        final TextView totalSp1 = (TextView) findViewById(R.id.tvSpiritual);
        final TextView totalEn1 = (TextView) findViewById(R.id.tvEnvironmental);
        final TextView totalEm1 = (TextView) findViewById(R.id.tvEmotional);
        final TextView totalSo1 = (TextView) findViewById(R.id.tvSocial);
        final Button bGraph = (Button)findViewById(R.id.weeklyGraph_btn);

        Intent graphIntent = getIntent();

        String name = graphIntent.getStringExtra("name");
        String username = graphIntent.getStringExtra("username");
        String age = graphIntent.getStringExtra("age");
        String email = graphIntent.getStringExtra("email");
        String total = graphIntent.getStringExtra("total");
        String totalP = graphIntent.getStringExtra("totalP");
        String totalI = graphIntent.getStringExtra("totalI");
        String totalSp = graphIntent.getStringExtra("totalSp");
        String totalEn = graphIntent.getStringExtra("totalEn");
        String totalEm = graphIntent.getStringExtra("totalEm");
        String totalSo = graphIntent.getStringExtra("totalSo");

        //used for intents
        intentName = name;
        intentUsername = username;
        intentAge = age;
        intentEmail = email;




        Log.d("s", "total" + total);
        Log.d("s", "totalP" + totalP);
        Log.d("s", "totalI" + totalI);
        Log.d("s", "totalSp" + totalSp);
        Log.d("s", "totalEn" + totalEn);
        Log.d("s", "totalEm" + totalEm);
        Log.d("s", "totalSo" + totalSo);

        total1.setText(total);
        totalP1.setText(totalP);
        totalI1.setText(totalI);
        totalSp1.setText(totalSp);
        totalEn1.setText(totalEn);
        totalEm1.setText(totalEm);
        totalSo1.setText(totalSo);

        configureBioButton();

        bGraph.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent bioIntent = getIntent();

                final String username = bioIntent.getStringExtra("username");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String totalW = jsonResponse.getString("total");
                                String totalPW = jsonResponse.getString("totalP");
                                String totalIW = jsonResponse.getString("totalI");
                                String totalSpW = jsonResponse.getString("totalSp");
                                String totalEnW = jsonResponse.getString("totalEn");
                                String totalEmW = jsonResponse.getString("totalEm");
                                String totalSoW = jsonResponse.getString("totalSo");


                                Log.d("asd", "totalW" + totalW);
                                Log.d("asd", "totalPW" + totalPW);
                                Log.d("asd", "totalIW" + totalIW);
                                Log.d("asd", "totalSpW" + totalSpW);
                                Log.d("asd", "totalEnW" + totalEnW);
                                Log.d("asd", "totalEmW" + totalEmW);
                                Log.d("asd", "totalSoW" + totalSoW);


                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent graphWIntent = new Intent(Graph.this, GraphWeekly.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                graphWIntent.putExtra("totalW", totalW);
                                graphWIntent.putExtra("totalPW", totalPW);
                                graphWIntent.putExtra("totalIW", totalIW);
                                graphWIntent.putExtra("totalSpW", totalSpW);
                                graphWIntent.putExtra("totalEnW", totalEnW);
                                graphWIntent.putExtra("totalEmW", totalEmW);
                                graphWIntent.putExtra("totalSoW", totalSoW);
                                graphWIntent.putExtra("name", intentName);
                                graphWIntent.putExtra("username", intentUsername);
                                graphWIntent.putExtra("age", intentAge);
                                graphWIntent.putExtra("email", intentEmail);



                                //startActivity(intent);
                                Graph.this.startActivity(graphWIntent);

                            }else{
                                String totalW = null;
                                String totalPW = null;
                                String totalIW = null;
                                String totalSpW = null;
                                String totalEnW = null;
                                String totalEmW = null;
                                String totalSoW = null;

                                Intent graphWIntent = new Intent(Graph.this, GraphWeekly.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                graphWIntent.putExtra("totalW", totalW);
                                graphWIntent.putExtra("totalPW", totalPW);
                                graphWIntent.putExtra("totalIW", totalIW);
                                graphWIntent.putExtra("totalSpW", totalSpW);
                                graphWIntent.putExtra("totalEnW", totalEnW);
                                graphWIntent.putExtra("totalEmW", totalEmW);
                                graphWIntent.putExtra("totalSoW", totalSoW);
                                graphWIntent.putExtra("name", intentName);
                                graphWIntent.putExtra("username", intentUsername);
                                graphWIntent.putExtra("age", intentAge);
                                graphWIntent.putExtra("email", intentEmail);

                                Log.d("f", "totalW" + totalW);
                                Log.d("f", "totalPW" + totalPW);
                                Log.d("f", "totalIW" + totalIW);
                                Log.d("f", "totalSpW" + totalSpW);
                                Log.d("f", "totalEnW" + totalEnW);
                                Log.d("f", "totalEmW" + totalEmW);
                                Log.d("f", "totalSoW" + totalSoW);


                                //startActivity(intent);
                                Graph.this.startActivity(graphWIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                WeeklyGraphRequest weeklyGraphRequest = new WeeklyGraphRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(Graph.this);
                queue.add(weeklyGraphRequest);
            }

        });
    }
    private void configureBioButton() {
        Button bioButton = (Button) findViewById(R.id.bio_btn);
        bioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent graphIntent = getIntent();

                final String name = graphIntent.getStringExtra("name");
                final String username = graphIntent.getStringExtra("username");
                final String age = graphIntent.getStringExtra("age");
                final String email = graphIntent.getStringExtra("email");

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                //this creates a new string variable that holds the user's name
                                String points = jsonResponse.getString("total");



                                //Log.d("asd", "name" + username);
                                //Log.d("asd", "name" + age);
                                //Log.d("asd", "name" + email);

                                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen)
                                Intent bioIntent = new Intent(Graph.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                Graph.this.startActivity(bioIntent);

                            }else{
                                String points = null;


                                Intent bioIntent = new Intent(Graph.this, BioPage.class);
                                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                                bioIntent.putExtra("name", intentName);
                                bioIntent.putExtra("username", intentUsername);
                                bioIntent.putExtra("age", intentAge);
                                bioIntent.putExtra("email", intentEmail);
                                bioIntent.putExtra("points", points);



                                //startActivity(intent);
                                Graph.this.startActivity(bioIntent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                BioPointRequest bioPointRequest = new BioPointRequest(username, responseListener);
                RequestQueue queue = Volley.newRequestQueue(Graph.this);
                queue.add(bioPointRequest);
            }

        });
    }
}
