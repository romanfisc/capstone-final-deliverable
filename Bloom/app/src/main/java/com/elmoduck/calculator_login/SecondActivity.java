package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    String name;
    String username;
    String age;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");





        configureBioButton();

        configureActivityButton();

    }
    private void configureBioButton() {
        Button bioButton = (Button) findViewById(R.id.bio_btn);
        bioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //linking to another activity.
                //startActivity(new Intent(SecondActivity.this, BioPage.class));
                //this creates a new string variable that holds the user's name
                //Intent intent = new Intent(SecondActivity.this, BioPage.class);
                Intent intent = getIntent();
                name = intent.getStringExtra("name");
                username = intent.getStringExtra("username");
                age = intent.getStringExtra("age");
                email = intent.getStringExtra("email");

                Log.d("asd", "name" + name);
                Log.d("asd", "name" + username);
                Log.d("asd", "name" + age);
                Log.d("asd", "name" + email);

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent bioIntent = new Intent(SecondActivity.this, BioPage.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                bioIntent.putExtra("name", name);
                bioIntent.putExtra("username", username);
                bioIntent.putExtra("age", age);
                bioIntent.putExtra("email", email);



                SecondActivity.this.startActivity(bioIntent);
            }
        });
    }
    private void configureActivityButton() {
        Button activitiesButton = (Button) findViewById(R.id.activities_btn);
        activitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                startActivity(new Intent(SecondActivity.this, ActivitiesNearMe.class));
            }
        });
    }

}