package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CouponDeactivate extends AppCompatActivity {

    String company_name;
    String username;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_deactivate);


        configureManageButton();



    }

    private void configureManageButton() {
        Button couponButton = (Button) findViewById(R.id.couponManageReturnBtn);
        couponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //linking to another activity.
                //startActivity(new Intent(SecondActivity.this, BioPage.class));
                //this creates a new string variable that holds the user's name
                //Intent intent = new Intent(SecondActivity.this, BioPage.class);
                Intent couponDeactivateIntent = getIntent();
                company_name = couponDeactivateIntent.getStringExtra("company_name");//pull the value of the vendor id, which was converted to a string in the previous java class
                username = couponDeactivateIntent.getStringExtra("username");
                email = couponDeactivateIntent.getStringExtra("email");

                //Log.d("asd", "name" + company_name);
                //Log.d("asd", "name" + username);
                //Log.d("asd", "name" + email);

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent manageIntent = new Intent(CouponDeactivate.this, CouponManage.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                manageIntent.putExtra("company_name", company_name);
                manageIntent.putExtra("username", username);
                manageIntent.putExtra("email", email);



                CouponDeactivate.this.startActivity(manageIntent);
            }
        });
    }
}

