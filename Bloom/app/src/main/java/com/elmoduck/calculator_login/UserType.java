package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class UserType extends AppCompatActivity {

    Boolean shouldAllowBack = false;

    @Override
    public void onBackPressed() {
        if (!shouldAllowBack) {
            //doSomething();
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);

        configureUserButton();

        configureVendorButton();
    }

    private void configureUserButton() {
        Button userButton = (Button) findViewById(R.id.user_btn);
        userButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                startActivity(new Intent(UserType.this, MainActivity_Login.class));
            }
        });
    }
    private void configureVendorButton() {
        Button vendorButton = (Button) findViewById(R.id.vendor_btn);
        vendorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                startActivity(new Intent(UserType.this, VendorLogin.class));
            }
        });
    }

}