package com.elmoduck.calculator_login;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CouponCreate extends AppCompatActivity {

    String company_name;
    String username;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_create);


        final EditText etTitle = (EditText) findViewById(R.id.etTitle);
        final EditText etDescription = (EditText) findViewById(R.id.etDescription);
        final EditText etPointTotal = (EditText) findViewById(R.id.etPointTotal);
        final EditText etCouponCode = (EditText) findViewById(R.id.etCouponCode);

        final Button bAdd = (Button) findViewById(R.id.bAdd);


        Intent createCouponIntent = getIntent();


        username = createCouponIntent.getStringExtra("username");
        company_name = createCouponIntent.getStringExtra("company_name");
        email = createCouponIntent.getStringExtra("email");

        configureVendorHomeButton();
        configureVendorSettingsButton();



        Log.d("bsd", "name" + username);



        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent createCouponIntent = getIntent();


                final String username = createCouponIntent.getStringExtra("username");
                final String company_name = createCouponIntent.getStringExtra("company_name");
                final String email = createCouponIntent.getStringExtra("email");

                final String title = etTitle.getText().toString();
                final String description = etDescription.getText().toString();
                final int point_total = Integer.parseInt(etPointTotal.getText().toString());
                final String coupon_code = etCouponCode.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Intent manageIntent = new Intent(CouponCreate.this, CouponManage.class);
                        manageIntent.putExtra("company_name", company_name);
                        manageIntent.putExtra("username", username);
                        manageIntent.putExtra("email", email);
                        CouponCreate.this.startActivity(manageIntent);
                    }
                };

                CouponRequest registerRequest = new CouponRequest(username, title, description, point_total, coupon_code, responseListener);
                RequestQueue queue = Volley.newRequestQueue(CouponCreate.this);
                queue.add(registerRequest);
            }
        });
    }

    private void configureVendorHomeButton() {
        ImageButton homeButton = (ImageButton) findViewById(R.id.vendorhome_btn);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent manageIntent = new Intent(CouponCreate.this, CouponManage.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                manageIntent.putExtra("company_name", company_name);
                manageIntent.putExtra("username", username);
                manageIntent.putExtra("email", email);


                Log.d("bsd", "company_name" + company_name);
                Log.d("bsd", "username" + username);
                Log.d("bsd", "email" + email);


                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                CouponCreate.this.startActivity(manageIntent);
            }
        });
    }

    private void configureVendorSettingsButton() {
        ImageButton settingsButton = (ImageButton) findViewById(R.id.vendorsettings_btn);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //here a new intent is create; this allows for the transition of activities (rooms, views, whatever you want to call a new screen
                Intent settingsIntent = new Intent(CouponCreate.this, VendorSettings.class);
                //this addition part takes the name that we pulled from the query and adds it to the next activity that we go to.
                settingsIntent.putExtra("company_name", company_name);
                settingsIntent.putExtra("username", username);
                settingsIntent.putExtra("email", email);


                Log.d("VSB", "company_name" + company_name);
                Log.d("VSB", "username" + username);
                Log.d("VSB", "email" + email);


                //startActivity is just Android's way of starting to switch screens. This has nothing to do with me naming the button 'activitiesButton'.
                CouponCreate.this.startActivity(settingsIntent);
            }
        });
    }
}