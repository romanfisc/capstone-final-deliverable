package com.elmoduck.calculator_login;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Elijah on 3/24/2018.
 */

public class UpdateUserPoints extends StringRequest {
    private static final String ADD_POINTS_URL = "http://cgi.soic.indiana.edu/~eliduckw/bloom/add_points.php";
    private Map<String, String> params;

    public UpdateUserPoints (String username, int points, int location_id, Response.Listener<String> listener){
        super(Method.POST, ADD_POINTS_URL, listener,null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("points_owed", points + "");
        params.put("location_id", location_id + "");
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}